import asyncio
import logging
import argparse

from os import path
from logging.handlers import RotatingFileHandler

import settings
from tram import Tram

_DIR = path.dirname(__file__)


async def run_tram(tram_id: int, route_id: int):
    tram = Tram(tram_id, route_id)

    while True:
        await tram.move()


def init_logging(file_name: str = 'tram_emulator.log'):
    filename = path.join(_DIR, file_name)
    logfile = RotatingFileHandler(
        filename=filename,
        maxBytes=1024 * 1024,
        backupCount=3,
        encoding='utf-8'
    )
    logfile.setLevel(logging.INFO)

    console = logging.StreamHandler()
    console.setLevel(logging.INFO)
    logging.basicConfig(
        level=logging.INFO,
        format='%(asctime)s|tram_emulator|%(filename)20s[%(lineno)3s] %(levelname)8s : %(message)s',
        handlers=[logfile, console]
    )

    logging.basicConfig(level=logging.DEBUG)


def main(*args):
    tram_id = int(args[0].tram_id)
    route_id = int(args[0].route_id)

    loop = asyncio.get_event_loop()
    init_logging()

    try:
        loop.run_until_complete(
            run_tram(tram_id, route_id)
        )
    except Exception as e:
        loop.close()
        print(f'ERROR: {e}')
        logging.error(msg='Error', exc_info=e)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--tram_id')
    parser.add_argument('--route_id')

    arguments = parser.parse_args()

    main(arguments)
