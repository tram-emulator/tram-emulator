from configparser import ConfigParser


def read_config(file_name: str = 'config.ini'):
    config = ConfigParser()
    config.read(file_name)

    return config


_config = read_config()


TRAM_API_URL = _config['tram_api']['url']
