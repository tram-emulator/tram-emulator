import asyncio
import aiohttp
import logging

from yarl import URL
from functools import wraps
from configparser import ConfigParser

from tram.exceptions import TramAPIError
from tram.serializers import TramState


def handle_error():
    def wrapper(func):
        @wraps(func)
        async def wrapped(*args, **kwargs):
            try:
                response = await func(*args, **kwargs)
                if 'description' in response:
                    raise TramAPIError(response['description'])

                return response
            except aiohttp.ClientError as e:
                logging.exception(msg='Client connection error', exc_info=e)
                raise TramAPIError(e.args)
            except aiohttp.ServerConnectionError as e:
                logging.exception(msg='Server connection error', exc_info=e)
                raise TramAPIError(e.args)

        return wrapped
    return wrapper


class TramAPI:
    def __init__(self, url: str):
        self._url = URL(url)

    @handle_error()
    async def get_stops(self, route_id: int, **kwargs) -> list:
        path = f'stops/{route_id}'
        async with aiohttp.ClientSession() as session:
            async with session.get(self._prepare_url(path, **kwargs)) as response:
                return await response.json()

    def _prepare_url(self, path: str, **query) -> URL:
        url = self._url.with_path(path)
        if query:
            url = url.with_query(**query)
        return url

    @handle_error()
    async def update_state(self, tram_id: int, state: TramState):
        path = f'/tram-state/{tram_id}'
        async with aiohttp.ClientSession() as session:
            async with session.put(self._prepare_url(path), json=state.dict()) as response:
                return await response.json()

    async def get_state(self, tram_id: int) -> TramState:
        path = f'/tram-state/{tram_id}'
        async with aiohttp.ClientSession() as session:
            async with session.get(self._prepare_url(path)) as response:
                data = await response.json()
                return TramState(**data)


async def main():
    config = ConfigParser()
    config.read('config.ini')

    api_config = config['api']

    api = TramAPI(api_config['url'])

    # stops = await api.get_stops(30, direction='forward')
    # stops = await api.get_stops(30)
    # for _stop in stops:
    #     print(_stop)

    # stops = await api.get_stops(30)
    # for stop in stops:
    #     print(stop)

    # state = await api.get_state(17)
    # print(state)

    state = TramState(stop_id=0, route_id=500, direction='forward')

    await api.update_state(17, state)
    # await api.get(1)
    # await api.update(tram_id=1, direction='sad')

if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    loop.run_until_complete(main())
