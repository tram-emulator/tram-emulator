from enum import Enum
from pydantic import BaseModel, validator


class Direction(str, Enum):
    Forward = 'forward'
    Backward = 'backward'


class TramState(BaseModel):
    stop_id: int = 0
    route_id: int = 0
    direction: str = ''

    @validator('direction')
    def validate_direction(cls, value):
        direction = Direction(value)
        return direction.value
