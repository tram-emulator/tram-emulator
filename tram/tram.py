import asyncio
import logging

import settings

from tram.tram_api import TramAPI
from tram.serializers import Direction, TramState
from stop_info import StopTimings, NULL_STOP_ID


class Tram:
    def __init__(
            self,
            tram_id: int,
            route_id: int
    ):
        self._tram_id = tram_id
        self._tram_api = TramAPI(settings.TRAM_API_URL)
        self._tram_state = TramState(
            stop_id=NULL_STOP_ID,
            route_id=route_id,
            direction=Direction.Forward.value
        )

    async def move(self):
        self._tram_state = await self._tram_api.get_state(self._tram_id)
        stops = await self._tram_api.get_stops(
            self._tram_state.route_id,
            direction=self._tram_state.direction
        )

        await self._move_through_stops(stops)
        await self._update_direction()
        await self._tram_api.update_state(self._tram_id, self._tram_state)

    async def _move_through_stops(self, stops: list):
        for _stop in stops:
            logging.info(f"tram is in {_stop['name']}")

            self._tram_state.stop_id = _stop['id']
            await self._tram_api.update_state(self._tram_id, self._tram_state)
            await asyncio.sleep(StopTimings.StayOnStation)

            if _stop != stops[-1]:
                logging.info('-----moving-----')

            self._tram_state.stop_id = NULL_STOP_ID
            await self._tram_api.update_state(self._tram_id, self._tram_state)
            await asyncio.sleep(StopTimings.MoveToStation)

    async def _update_direction(self):
        if self._tram_state.direction == Direction.Forward:
            self._tram_state.direction = Direction.Backward
        else:
            self._tram_state.direction = Direction.Forward

        logging.info('-----DIRECTION IS CHANGED-----')
