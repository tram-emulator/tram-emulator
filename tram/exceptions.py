class TramAPIError(Exception):
    ...


class TramStateError(TramAPIError):
    ...


class GetStateError(TramStateError):
    ...


class UpdateStateError(TramStateError):
    ...
